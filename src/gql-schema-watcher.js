/**
	* This file watches for changes in graphql schema changes and will run `codegen`
	* script if it finds any changes
	*/

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// eslint-disable-next-line @typescript-eslint/no-var-requires,no-undef
const chokidar = require('chokidar');
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// eslint-disable-next-line @typescript-eslint/no-var-requires,no-undef
const { exec } = require('child_process');

// eslint-disable-next-line no-undef
const watcher = chokidar.watch(__dirname + '/components/**/*.graphql', {
	persistent: true,
});

const handler = path => {
	exec(`yarn codegen && eslint --fix ${__dirname}/generated/graphql.tsx`, (err, stdout) => {
		console.log(stdout);
	});
};

watcher
	.on('add', function (path) {
		handler(path);
	})
	.on('change', function (path) {
		handler(path);
	})
	.on('unlink', function (path) {
		handler(path);
	});

setTimeout(() => {
	console.info('Watching for gql file changes...');
}, 2000);
