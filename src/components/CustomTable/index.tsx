import Table from './Table';
import Thead from './THead';
import TD from './TD';
import TR from './TR';

export default Table;
export {
	Thead,
	TD,
	TR,
};
