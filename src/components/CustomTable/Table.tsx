import styled from 'styled-components';

interface ITableProps {
	striped?: boolean;
	theme?: 'dark' | 'light';
}

interface ITheme {
	[key: string]: {
		background: string;
		altBackground: string;
		color: string;
		hoverColor: string;
	}
}

const themeConfig :ITheme = {
	dark: {
		background: '#34495e',
		altBackground: '#34395a',
		color: '#fff',
		hoverColor: '#343055',
	},
	light: {
		background: '#fff',
		altBackground: '#F5F5F5',
		color: '#000',
		hoverColor: 'aqua',
	},
};

const getStripedStyleByTheme = (themeProp: string) =>
	`tr:nth-of-type(even) {	
				background:  ${themeConfig[themeProp].altBackground}
				}
				tr:hover {
				background-color: ${themeConfig[themeProp].hoverColor};
		}
				`;

const Table = styled.table<ITableProps>`
  margin: 1em 10px 0 10px;
		min-width: 300px;
  background: ${props => themeConfig[props.theme || 'light'].background};
  color: ${props => themeConfig[props.theme || 'light'].color};
  border-radius: 0.4em;
  overflow: hidden;
  padding: 1em 2em 1em 1em;
		${props => props.striped && getStripedStyleByTheme(props.theme)}
`;

export default Table;
