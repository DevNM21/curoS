import styled from 'styled-components';

const TD = styled.td`
  margin: 0.5em 1em;
		padding: 1em;
`;

export default TD;
