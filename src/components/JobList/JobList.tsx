// libraries
import React, { FC } from 'react';
import { Link } from 'react-router-dom';

// API queries
import { Job, JobListQuery } from '../../generated/graphql';

// Components
import Table, { TD, Thead, TR } from '../CustomTable';

interface Props {
	/** Array of Jobs from the API */
	data: JobListQuery;
}

const isJobRemote = (locationNames: Job['locationNames']) => {
	if (!locationNames)
		return true;
	else if (locationNames.includes('Remote'))
		return true;
	return false;
};
/**
 * Table to render jobs fetched from Jobs Query
 * @param data {JobListQuery}
 * @constructor
 */
const JobList: FC<Props> = ({ data }) => {
	return (
		<>
			<Table striped={true} theme={'dark'}>
				<Thead>
					<tr>
						<th>Title</th>
						<th>Company</th>
						<th>City</th>
						<th>Country</th>
						<th>Is Remote?</th>
						<th>View Job Details</th>
					</tr>
				</Thead>
				<tbody>
					{
						data && data.jobs.length && data.jobs.map(job => <TR key={job.id}>
							<TD data-th={'Title'}>{job.title}</TD>
							<TD data-th={'Company'}>{job.company?.name}</TD>
							<TD data-th={'City'}>{job?.cities?.length ? job.cities[0].name : '-'}</TD>
							<TD data-th={'Country'}>{job?.cities?.length ? job.cities[0].country.name : '-'}</TD>
							<TD data-th={'Is Remote?'}>{isJobRemote(job.locationNames) ? 'Yes': 'No'}</TD>
							<TD data-th={'View Job Details?'}>
								{
									job.company ?
										<Link to={`/job/${job.company.slug}/${job.slug}`}>
											View
										</Link>
										: 'Not available'
								}
							</TD>
						</TR>,
						)
					}
				</tbody>
			</Table>
		</>
	);
};

export default JobList;
