import * as React from 'react';
import { useJobListQuery } from '../../generated/graphql';
import JobsList from './JobList';
import { FC } from 'react';

/**
	* Container to isolate side effects
	* @constructor
	*/
const JobListContainer: FC = () => {
	const { data, error, loading } = useJobListQuery();

	if (!loading && !error && data)
		return <JobsList data={data} />;

	return <p>Loading...</p>;
};

export default JobListContainer;
