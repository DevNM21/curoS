import React, { FC } from 'react';

interface Props {
	title: string;
	description?: string | null;
	locationName?: string | null;
	commitment: string;
}

const JobDetails: FC<Props> = (props) => {
	const { title, description, commitment, locationName } = props;
	return <>
		<h1>{title}</h1>
		<p>{description || 'Description Not Provided'}</p>
		<p>{commitment}</p>
		<p>{locationName || 'Location not specified'}</p>
	</>;
};

export default JobDetails;
