import React, { FC } from 'react';
import JobDetails from 'components/JobDetails/JobDetails';
import { useFetchJobQuery } from '../../generated/graphql';

interface Props {
	companySlug: string;
	jobSlug: string;
}

const JobDetailsContainer: FC<Props> = (props) => {
	const { jobSlug, companySlug } = props;
	const { data, loading, error } = useFetchJobQuery({
		variables: {
			jobSlug,
			companySlug,
		}
	});
	
	if (loading)
		return <p>Loading the job details...</p>;
	
	if (error)
		return <p>Could not load job details due to: {error.message}</p>;
	
	if (data?.job)
		return <>
			<JobDetails
				title={data.job.title}
				description={data.job.description}
				locationName={data.job.locationNames}
				commitment={data.job.commitment.title}
			/>
		</>;

	return <p>No job found</p>;
};

export default JobDetailsContainer;
