import React, { FC } from 'react';
import { useParams } from 'react-router-dom';
import JobDetailsContainer from 'components/JobDetails';

const JobDetails: FC = () => {
	const { companySlug, jobSlug } = useParams<{companySlug: string; jobSlug: string}>();
	if (!jobSlug || !companySlug)
		return <p>Invalid URL</p>;

	return <>
		<JobDetailsContainer companySlug={companySlug} jobSlug={jobSlug} />
	</>;
};


export default JobDetails;
