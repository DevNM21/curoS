import React from 'react';
import JobList from 'components/JobList';
import { Container } from 'components/Container';
const Home = () => {
	return <>
		<Container>
			<h1>Job Listings</h1>
			<JobList/>
		</Container>
	</>;
};

export default Home;
