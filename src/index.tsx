import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import App from './routes/index';

const client = new ApolloClient({
	uri: 'https://api.graphql.jobs/',
	cache: new InMemoryCache(),
});

ReactDOM.render(
	<ApolloProvider client={client}>
		<React.StrictMode>
			<App/>
		</React.StrictMode>
	</ApolloProvider>,
	document.getElementById('root')
);
