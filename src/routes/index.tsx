// libraries
import React, { ReactElement } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

// Pages
import Home from 'views/home';
import JobDetails from 'views/job-details';

const IndexRouter: React.FC = (): ReactElement => {
	return (
		<Router>
			<Routes>
				<Route path='/' element={<Home />} />
				<Route path='/job/:companySlug/:jobSlug' element={<JobDetails />} />
			</Routes>
		</Router>
	);
};

export default IndexRouter;
